# soal-shift-sisop-modul-3-D08-2022

|          Nama          |      NRP   |
| :------------------    | :----------|
| Theresia Nawangsih     | 5025201144 |


## Kendala :
Dalam kelompok hanya satu orang yang mengerjakan semua rangkaian pratikum. Anggota satu afk (tiada kabar) dan anggota yang lain tidak jelas

# Soal 1
Pada soal ini diminta untuk mendownload dua file zip dan unzip file tersebut pada dua folder berbeda yaitu `quote.zip` dan `music.zip`. Dan untuk unzip dilakukan bersaman dengan mengunkan thread. lalu mendecode semua `file.txt` yang ada dengan base 64 dan memasukkan hasil `file.txt` yang baru  dalam folder dengan hasil dua `file.txt` .  Selanjutnya, memindahkan kedua `file.txt` yang berisi hasil decoding ke folder yang baru dengan nama `hasil`. Folder hasil `di-zip` dengan password `mihinomenest[namauser]`. Terakhir, unzip file hasil.zip dan membuat file `no.txt`, dengan tulisan `No` pada saat yang bersamaan, lalu zip kedua hasil file hasil dan menjadi `hasil.zip` dengan password yang sama seperti sebelumnya.

## A. Download Dua File Zip dan Unzip File Zip
Pertama, disini mempunyai link yang digunakan untuk mendownload file tersebut dengan
```
char *links[] = {
    "https://drive.google.com/uc?export=download&id=1hKJmC2BJH7PLg5nCRPNuSaWSrD4fg1eW",
    "https://drive.google.com/uc?export=download&id=1mjdbT-Q_k5I_9tXmdk46wmDRmbPYboEY"
};
```
dilanjutkan dengan membuat `void download`
```
void download(){
	pid_t new_id;
	for(int i = 0; i < 2; i++){
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			// nama + .zip
			char name[strlen(names[i]) + strlen(".zip") + 1];
			strcpy(name, names[i]);
			strcat(name, ".zip");
			printf("Successfully downloaded %s directory\n",name);
			char *argv[] = {"wget", "-q", "--no-check-certificate",links[i],"-O",name,NULL};
			execv("/usr/bin/wget",argv);
		}
		waitpid(new_id,NULL,0);
	}

}
```
lalu, pada unzip juga membuat `void unzip`. Dan unzip dilakukan dengan bersaam menggunkan thread
```
void *unzip(void *index){
	int i = *((int*) index);
	pid_t new_id;
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		DIR *temp = opendir(names[i]);
		if(temp){
			int temp_id = fork();
			if(temp_id < 0) exit(EXIT_FAILURE);
			if(temp_id == 0){
				char *argv[] = {"rm","-r",names[i],NULL};
				execv("/usr/bin/rm",argv);
			}
			waitpid(temp_id,NULL,0);
			printf("Successfully deleted %s directory\n",names[i]);
		}
		char *argv[] = {"mkdir","-p",names[i],NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id,NULL,0);
	printf("Successfully created %s directory\n",names[i]);
	
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char name[strlen(names[i]) + strlen(".zip") + 1];
		strcpy(name, names[i]);
		strcat(name, ".zip");
		printf("Successfully unzipped %s to %s directory\n",name,names[i]);
		char *argv[] = {"unzip","-qq",name,"-d",names[i],NULL};
		execv("/usr/bin/unzip",argv);
	}
	waitpid(new_id,NULL,0);
	
}
```
maka untuk membuat directory dan juga unzip file dapat menggunakan `pthread_create`
```
pthread_create(&threads[0],NULL,unzip,(void *) &int_threads[0]);
	pthread_create(&threads[1],NULL,unzip,(void *) &int_threads[1]);
	for(int i = 0; i < 2; i++) pthread_join(threads[i], NULL);
```
- Tampilan ketika sudah mendowload 

![downloadsukses](https://drive.google.com/uc?export=view&id=19QB-8GF8sVioa5X4gHyPxVAsuiAoCVBy)

- Tampilan ketika sudah unzip file

![unzip](https://drive.google.com/uc?export=view&id=1ABJBEv69z95rMOQ-I6Za4fyWtxITO19q)

## B. Decode File.txt dengan Base 64
Pertama, terdapat `void decode`
```
void *decode(void *index){
	int i = *((int*) index);
	char name[strlen(names[i]) + strlen(".txt") + 1];
	strcpy(name, names[i]); strcat(name, ".txt");
	FILE *output = fopen(name, "w");
	int link[2];
	
	DIR *dir_scan;
	struct dirent *entry;
	if((dir_scan = opendir(names[i])) == NULL) exit(EXIT_FAILURE);
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0) continue;
		char *foo = (char*) malloc(sizeof(char) * 4096);
		if(pipe(link) == -1) exit(EXIT_FAILURE);
		pid_t new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char temp[strlen(entry->d_name) + strlen(names[i]) + 2];
			strcpy(temp, names[i]);
			strcat(temp,"/");
			strcat(temp, entry->d_name);
			
			dup2(link[1], STDOUT_FILENO);
			close(link[0]);close(link[1]);
			
			char *argv[] = {"base64","-d",temp,NULL};
			execv("/usr/bin/base64",argv);
		}
		waitpid(new_id,NULL,0);
		close(link[1]);
		read(link[0],foo,4096);
		fprintf(output, "%s\n",foo);
	}
	fclose(output);
}
```
Karena pada ketentuan soal diminta dengan base 64 maka dapat menggunakan dengan 
```
char *argv[] = {"base64","-d",temp,NULL};
			execv("/usr/bin/base64",argv);
```
Dilanjutkan, dengan menggunakan `pthread_create` dapat membuat decode
```
pthread_create(&threads[0],NULL,decode,(void *) &int_threads[0]);
	pthread_create(&threads[1],NULL,decode,(void *) &int_threads[1]);
	for(int i = 0; i < 2; i++) pthread_join(threads[i], NULL);
```
- Tampilan quote.txt dan music.txt

![quotetxt](https://drive.google.com/uc?export=view&id=1jQDYjFrkhsN6pg8tJw3Abc9AF4h6jLRG)

![musictxt](https://drive.google.com/uc?export=view&id=1gpajrdCHEHBGwnWqyXIYGLeO6l7FGoCP)

- Tampilan Isi


![isiquote](https://drive.google.com/uc?export=view&id=1G5jyCNciFokvgOh2PhkNvc5PM0ylqjj)


![isimusic](https://drive.google.com/uc?export=view&id=12l7DdpjROjrwX6WeXyPzAHWjWRlQ7-Sx)


## C. Memindahkan Kedua file.txt pada Folder baru bernama hasil
Pertama, terdapat `void hasil` yang digunakan untuk membuat directory hasil
```
void hasil(){
	// mkdir hasil
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		DIR *temp = opendir("hasil");
		if(temp){
			int temp_id = fork();
			if(temp_id < 0) exit(EXIT_FAILURE);
			if(temp_id == 0){
				char *argv[] = {"rm","-r","hasil",NULL};
				execv("/usr/bin/rm",argv);
			}
			waitpid(temp_id,NULL,0);
			printf("Successfully deleted hasil directory\n");
		}
		char *argv[] = {"mkdir","-p","hasil",NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id, NULL, 0);
// hasil -> hasil directory
	for(int i = 0; i < 2; i++){
		char *name = (char*) malloc(strlen(names[i]) + strlen(".txt") + 1);
		strcpy(name, names[i]); strcat(name, ".txt");
		char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
		strcpy(dirtemp, cur_dir);
		strcat(dirtemp,"/");strcat(dirtemp,"hasil");
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"mv",name,dirtemp,NULL};
			execv("/usr/bin/mv",argv);
		}
		waitpid(new_id,NULL,0);
	}
```
lalu memindahkan hasil dicoding pada directory hasil
```
hasil(); 
	zip_hasil(password);
```

- Tampilan pada directory hasil

![direchasil](https://drive.google.com/uc?export=view&id=1djmXSGogBquFBDW2In5Kd9-ZUR3VooVh)


![isihasil](https://drive.google.com/uc?export=view&id=1CQnRoqrK0_49JCIb51UXH8DMzcDeeMkr)



## D. Folder Hasil Menjadi File Hasil.zip dengan Password
Disini saya, membuat `void zip hasil` dan juga `void unzip hasil`
```
void zip_hasil()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
	puts("Successfully zipped hasil to hasil.zip");
}

int unzipped_hasil = 0;

void *unzip_hasil(void *_password){
	char *password = (char*) _password;
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"unzip","-q","-P",password,"hasil.zip",NULL};
		execv("/usr/bin/unzip",argv);
	}
	waitpid(new_id,NULL,0);
	unzipped_hasil = 1;
}
```
dikarenakan pada soal diharuskan dengan password maka disini saya membuat password dengan nama password `mihinomenesttheresia`
```
uid_t uid;
	struct passwd *pw;
	uid = geteuid();
	pw = getpwuid(uid);
	if(pw == NULL) exit(EXIT_FAILURE);
	char password[strlen(pw->pw_name) + strlen("mihinomenesttheresia") + 1];
	strcpy(password,"mihinomenesttheresia");strcat(password, pw->pw_name);
```
- Tampilan hasil.zip dengan password


![pwd](https://drive.google.com/uc?export=view&id=1HYvyOzE8pZDPu64rgwru81UtVMD65X4b)


## E. File No.txt
Pada file no.txt ini juga, saya membuat void yaitu `void no_txt`
```
void *no_txt(){
	FILE *no = fopen("no.txt","w");
	fprintf(no,"No\n");
	fclose(no);
	DIR *hasil;
	while(!unzipped_hasil){
	}
	char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
	strcpy(dirtemp, cur_dir);
	strcat(dirtemp,"/");strcat(dirtemp,"hasil");
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"mv","no.txt",dirtemp,NULL};
		execv("/usr/bin/mv",argv);
	}
	waitpid(new_id,NULL,0);
}
```
dan juga disini saya menggunakan `pthread_create` untuk memasukkan no.txt pada hasil 
```
pthread_create(&threads[0],NULL,no_txt,NULL);
	pthread_create(&threads[1],NULL,unzip_hasil,(void*)password);
	for(int i = 0; i < 2; i++) pthread_join(threads[i],NULL);
	zip_hasil(password);
```
dan juga memberikan password yang sama seperti sebelumnya yaitu `mihinomenesttheresia`

- Tampilan no.txt


![notxt](https://drive.google.com/uc?export=view&id=1LJJFbyNi1yUKeCir0hwMM2Ro5_rDg-5R)


### Kendala Soal no 1
saat memasukkan password pada hasil.zip pertama tidak dapat terdetec password yang dimasukkan

# Soal 3
Pada soal no 3 ini, diminta untuk mencoba merapikan file bernama harta karun berdasarkan jenis/tipe/kategori/ekstensinya. mengextract zip yang diberikan dalam folder `/home/[user]/shift3/`. Lalu working directory program akan berada dalam folder `/home/[user]/shift3/hartakarun/`. Semua file harus berada dalam folder, jika terdapat file yang tidak memiliki eksistensi, maka file akan disimpan dalam folder `unknown` jika file hidden makan akan disimpan dalam folder `hidden`. Agara proses kategori dapat berjalan dengan cepat, maka setiap 1 file yang dikategorikan akan dioperasika oleh 1 thread. Nami menggunakan progra client-server. Saat progra `client` dijalan kan , maka folder `/home/[user]/shift3/hartakarun/`akan di-zip terlebih dhulu. `client` dapat mengirim file `hartakarun.zip` ke server.

## A. Extract Zip
Pertama akan membuat void extract, dengan menggunakan fungsi `popen` dengan argumen pertamaya adalah value dan argumen kedua adalah r.
```
void extract(char *value) 
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer,'\0',sizeof(buffer));

    file = popen(value ,"r");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n",buffer);
        pclose(file);
    }
}
```

- Tampilan extract zip

![tampilanisi](https://drive.google.com/uc?export=view&id=1FyjLlRR1dyX2j6UH9Nr6gd52WByqwEAA)


- Proses extract zip

![proses](https://drive.google.com/uc?export=view&id=1A9zjbaREOS8ZIM32WSUNk646LPTnZGAe)


## B. Kategori file Unknown dan Hidden
Pertama akan membuat `void sortfile` untuk mengkategorikan file
```
void sortFile(char *from)
{
    struct_path s_path;
    struct dirent *dp;
    DIR *dir;

    flag = 1;
    int index = 0;
    strcpy(s_path.cwd, "/home/theresianwg/shift3/hartakarun");

    dir = opendir(from);

    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char fileName[10000];
            sprintf(fileName, "%s/%s", from, dp->d_name);
            strcpy(s_path.from, fileName);

            pthread_create(&tid[index], NULL, move, (void *)&s_path);
            sleep(1);
            index++;
        }
    }
}
```
lalu program akan membaca directory hasil extract file.zip dan akan dicek satu-satu. Untuk informasi mengenai directory asal dan nama file akan disimpan pada variabel `filename`

- Tampilan Kategori Unknown

![tampilanunk](https://drive.google.com/uc?export=view&id=1UHwyxDXpR2qP6eBonWsoFXrdZeDYsxNO)


![rununk](https://drive.google.com/uc?export=view&id=1BVo27tDMVxXzE0EIWqkwXASqjtdJnHYf)


- Tampilan Kategori Hidden

![runhid](https://drive.google.com/uc?export=view&id=1uNn0E1WUT-i1MkIacbvn_ECsGlsL0UFd)

## C. Kategori File menggunakan Thread
Pada kali ini menggunakan fungsi `sortfile` dan memamnggil fungsi `move`
```
pthread_create(&tid[index], NULL, move, (void *)&s_path);
```
lalu menggunakan 
```
void *move(void *s_path)
{
    struct_path s_paths = *(struct_path*) s_path;
    moveFile(s_paths.from, s_paths.cwd);
    pthread_exit(0);
}
```
dan dilanjutkan dengan `void movefile`
```
void moveFile(char *p_from, char *p_cwd)
{
    char file_name[300], file_ext[30], temp[300];
    char *buffer;

    strcpy(temp, p_from);
    buffer = strtok(temp, "/");

    while(buffer != NULL){
        if(buffer != NULL) strcpy(file_name, buffer);
        buffer = strtok(NULL, "/");
    }

    strcpy(temp, file_name);
    
    buffer = strtok(temp, ".");
    buffer = strtok(NULL, "");

    if(file_name[0] == '.'){
        strcpy(file_ext, "Hidden");
    }else if(buffer != NULL){
        strcpy(file_ext, buffer);
    }else{
        strcpy(file_ext, "Unknown");
    }

    toLower(file_ext);

    // new dest
    strcat(p_cwd, "/");
    strcat(p_cwd, file_ext);
    strcat(p_cwd, "/");
    strcat(p_cwd, file_name);

    // information
    printf("from %s: \nfile_name: %s\nfile_ext: %s\ndest: %s\n\n", p_from, file_name, file_ext, p_cwd);

    // create dir and move file
    createDir(file_ext);
    rename(p_from, p_cwd);
}
```
dengan bantuan variabel `buffer` yang sebelumnya akan diisi nama file dengan bantuan fungsi `strtok` untum mencari delimiter dengn `/`. Lalu, menentukan eksistensi file dengan bantuan variabel `buffer`  yang berisi formt file stelah diinisiasi hasil dari `strtok` dengan delimiter `.`. Tetapi, terdapat pengecualian jika `NULL` maka artinya file dikategorikan hidden dan jika tidak memenehui keduannya akan dikategorikan `unkown`. Dilanjutkan dengan membuat destinasi dari file, juga menampilkan informasi file dan membuat directory berdasarkan eksistensi filenya.

- Tampilan eksistensi dan informasi file

![eksistensi](https://drive.google.com/uc?export=view&id=1tZDnvGb_Y8GC7-5CM41WWwV8GPF63Kek)

![infofile](https://drive.google.com/uc?export=view&id=1mKEqLD8-sFfCw_KUIAv6qM8JotqkXRX3)

## D. Folder akan di Zip (hartakarun.zip)
Disini akan menggunakan `void zip` yang akan menerima parameter dengan berupa command yang dieksekusi pada terminal. Command ini selanjutnya akan dieksekusi dengan popen yang ada dalam fungsi `zip`
```
void zip(char *ziping)
{
    FILE *file;
    char buffer[BUFSIZ+1];
    int chars_read;

    memset(buffer, '\0', sizeof(buffer));

    //ziping
    file = popen(ziping, "w");

    if(file != NULL){
        chars_read = fread(buffer, sizeof(char), BUFSIZ, file);
        if(chars_read > 0) printf("%s\n", buffer);
        pclose(file);
    }
}
```

- Tampilan hartakarun.zip

![tampilanfldr](https://drive.google.com/uc?export=view&id=1W8642FPTIcHMFOIuTqPsIxEP3vnyshbP)

### Kendala Soal no 3
file pada folder hidden kosong
