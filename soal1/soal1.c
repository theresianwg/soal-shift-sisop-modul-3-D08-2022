#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <memory.h>
#include <errno.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <pwd.h>
#include <time.h>

#define PASSWORD "mihinomenesttheresia"

char *decoding_table;
char cur_dir[256];
char music[256][256];
char quote[256][256];
int music_count = 0, quote_count = 0;
pthread_t tid[10];
pid_t child_process;
char *names[] = {"quote","music"};
char *links[] = {
    "https://drive.google.com/uc?export=download&id=1hKJmC2BJH7PLg5nCRPNuSaWSrD4fg1eW",
    "https://drive.google.com/uc?export=download&id=1mjdbT-Q_k5I_9tXmdk46wmDRmbPYboEY"
};
void download(){
	pid_t new_id;
	for(int i = 0; i < 2; i++){
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			// nama + .zip
			char name[strlen(names[i]) + strlen(".zip") + 1];
			strcpy(name, names[i]);
			strcat(name, ".zip");
			printf("Successfully downloaded %s directory\n",name);
			char *argv[] = {"wget", "-q", "--no-check-certificate",links[i],"-O",name,NULL};
			execv("/usr/bin/wget",argv);
		}
		waitpid(new_id,NULL,0);
	}

}

void *unzip(void *index){
	int i = *((int*) index);
	pid_t new_id;
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		DIR *temp = opendir(names[i]);
		if(temp){
			int temp_id = fork();
			if(temp_id < 0) exit(EXIT_FAILURE);
			if(temp_id == 0){
				char *argv[] = {"rm","-r",names[i],NULL};
				execv("/usr/bin/rm",argv);
			}
			waitpid(temp_id,NULL,0);
			printf("Successfully deleted %s directory\n",names[i]);
		}
		char *argv[] = {"mkdir","-p",names[i],NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id,NULL,0);
	printf("Successfully created %s directory\n",names[i]);
	
	new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char name[strlen(names[i]) + strlen(".zip") + 1];
		strcpy(name, names[i]);
		strcat(name, ".zip");
		printf("Successfully unzipped %s to %s directory\n",name,names[i]);
		char *argv[] = {"unzip","-qq",name,"-d",names[i],NULL};
		execv("/usr/bin/unzip",argv);
	}
	waitpid(new_id,NULL,0);
	
}

void *decode(void *index){
	int i = *((int*) index);
	char name[strlen(names[i]) + strlen(".txt") + 1];
	strcpy(name, names[i]); strcat(name, ".txt");
	FILE *output = fopen(name, "w");
	int link[2];
	
	DIR *dir_scan;
	struct dirent *entry;
	if((dir_scan = opendir(names[i])) == NULL) exit(EXIT_FAILURE);
	while((entry = readdir(dir_scan)) != NULL){
		if(strcmp(".",entry->d_name) == 0 || strcmp("..",entry->d_name) == 0) continue;
		char *foo = (char*) malloc(sizeof(char) * 4096);
		if(pipe(link) == -1) exit(EXIT_FAILURE);
		pid_t new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char temp[strlen(entry->d_name) + strlen(names[i]) + 2];
			strcpy(temp, names[i]);
			strcat(temp,"/");
			strcat(temp, entry->d_name);
			
			dup2(link[1], STDOUT_FILENO);
			close(link[0]);close(link[1]);
			
			char *argv[] = {"base64","-d",temp,NULL};
			execv("/usr/bin/base64",argv);
		}
		waitpid(new_id,NULL,0);
		close(link[1]);
		read(link[0],foo,4096);
		fprintf(output, "%s\n",foo);
	}
	fclose(output);
}

void hasil(){
	// mkdir hasil
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		DIR *temp = opendir("hasil");
		if(temp){
			int temp_id = fork();
			if(temp_id < 0) exit(EXIT_FAILURE);
			if(temp_id == 0){
				char *argv[] = {"rm","-r","hasil",NULL};
				execv("/usr/bin/rm",argv);
			}
			waitpid(temp_id,NULL,0);
			printf("Successfully deleted hasil directory\n");
		}
		char *argv[] = {"mkdir","-p","hasil",NULL};
		execv("/usr/bin/mkdir",argv);
	}
	waitpid(new_id, NULL, 0);

	// hasil -> hasil directory
	for(int i = 0; i < 2; i++){
		char *name = (char*) malloc(strlen(names[i]) + strlen(".txt") + 1);
		strcpy(name, names[i]); strcat(name, ".txt");
		char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
		strcpy(dirtemp, cur_dir);
		strcat(dirtemp,"/");strcat(dirtemp,"hasil");
		new_id = fork();
		if(new_id < 0) exit(EXIT_FAILURE);
		if(new_id == 0){
			char *argv[] = {"mv",name,dirtemp,NULL};
			execv("/usr/bin/mv",argv);
		}
		waitpid(new_id,NULL,0);
	}
}

void zip_hasil()
{
  int status;
  child_process = fork();
  if (child_process == 0)
  {
    char *cmd[] = {"zip", "-P", PASSWORD, "-r", "./hasil.zip", "-q", "./hasil", NULL};
    execv("/bin/zip", cmd);
  }
  while ((wait(&status)) > 0)
    ;
	puts("Successfully zipped hasil to hasil.zip");
}

int unzipped_hasil = 0;

void *unzip_hasil(void *_password){
	char *password = (char*) _password;
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"unzip","-q","-P",password,"hasil.zip",NULL};
		execv("/usr/bin/unzip",argv);
	}
	waitpid(new_id,NULL,0);
	unzipped_hasil = 1;
}

void *no_txt(){
	FILE *no = fopen("no.txt","w");
	fprintf(no,"No\n");
	fclose(no);
	DIR *hasil;
	while(!unzipped_hasil){
	}
	char *dirtemp = (char*) malloc(strlen(cur_dir) + strlen("hasil") + 2);
	strcpy(dirtemp, cur_dir);
	strcat(dirtemp,"/");strcat(dirtemp,"hasil");
	pid_t new_id = fork();
	if(new_id < 0) exit(EXIT_FAILURE);
	if(new_id == 0){
		char *argv[] = {"mv","no.txt",dirtemp,NULL};
		execv("/usr/bin/mv",argv);
	}
	waitpid(new_id,NULL,0);
}

int main(){
	getcwd(cur_dir, sizeof(cur_dir) - 1);
	
	download();
	
	pthread_t threads[2];
	int int_threads[2] = {0,1};
	
	//a (mkdir dan unzip)
	pthread_create(&threads[0],NULL,unzip,(void *) &int_threads[0]);
	pthread_create(&threads[1],NULL,unzip,(void *) &int_threads[1]);
	for(int i = 0; i < 2; i++) pthread_join(threads[i], NULL);
	
	//b (decode)
	pthread_create(&threads[0],NULL,decode,(void *) &int_threads[0]);
	pthread_create(&threads[1],NULL,decode,(void *) &int_threads[1]);
	for(int i = 0; i < 2; i++) pthread_join(threads[i], NULL);
	
	//d(pwd hasil.zip)
	uid_t uid;
	struct passwd *pw;
	uid = geteuid();
	pw = getpwuid(uid);
	if(pw == NULL) exit(EXIT_FAILURE);
	char password[strlen(pw->pw_name) + strlen("mihinomenesttheresia") + 1];
	strcpy(password,"mihinomenesttheresia");strcat(password, pw->pw_name);
	
	//c (pindah .txt  hasil decoding ke folder bru hasil)
	hasil(); 
	zip_hasil(password); 
	
	//e (no.txt)
	pthread_create(&threads[0],NULL,no_txt,NULL);
	pthread_create(&threads[1],NULL,unzip_hasil,(void*)password);
	for(int i = 0; i < 2; i++) pthread_join(threads[i],NULL);
	zip_hasil(password);
	
	
	return 0;
}
